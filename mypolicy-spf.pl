#!/usr/bin/perl -T
use strict;
use Mail::SPF;

use constant _MAX_BUF => 512;
my %attr;

#
# Usage: mypolicyd-spf.pl
#
# Delegated Postfix SMTPD policy server for SPF. Based on the Demo provided by 
# http://www.postfix.org/SMTPD_POLICY_README.html
#
# How it works: each time a Postfix SMTP server process is started it connects
# to the policy service socket. Postfix then runs one instance of this perl
# script.
#
# Each query is a bunch of key-value pairs line delimited:
#
#    request=smtpd_access_policy
#    protocol_state=RCPT
#    protocol_name=SMTP
#    helo_name=some.domain.tld
#    queue_id=8045F2AB23
#    sender=foo@bar.tld
#    recipient=bar@foo.tld
#    client_address=1.2.3.4
#    client_name=another.domain.tld
#    instance=123.456.7
#    sasl_method=plain
#    sasl_username=you
#    sasl_sender=
#    size=12345
#    [empty line]
#
# Order does not matter. The policy server script will answer in the same
# style, with an attribute list followed by an empty line:
#
# action=dunno
# [empty line]
#
# Uses Mail::SPF as documented here:
# http://search.cpan.org/~jmehnle/Mail-SPF-v2.9.0/lib/Mail/SPF.pm
#

# Unbuffer STDOUT
select((select(STDOUT), $| = 1)[0]);

# Main loop, read STDIN
while (<STDIN>) {
    if (/([^=]+)=(.*)\n/) {
        $attr{substr($1, 0, _MAX_BUF)} = substr($2, 0, _MAX_BUF);
    }
    elsif ($_ eq "\n") {
#        if ($verbose) {
#            ...
#        }

        exit(1) unless $attr{request} eq "smtpd_access_policy";

        # Determine action
        my $s = Mail::SPF::Server->new();
        my $r = Mail::SPF::Request->new(
            scope => 'mfrom',
            identity => $attr{sender},
            ip_address => $attr{client_address}
        );

        my $result = $s->process($r);
        
        # This fails open should an error occur. It will only hard reject on a
        # true "fail" code. This means if it errors out for w/e reason or there
        # is no SPF record set for the incoming domain it will "fail open" and
        # return "dunno" thus passing responsibility to the next restriction.
        #
        # See Postfix access(5) for action definitions.
        my $action = ($result->code eq 'fail') ? 'reject' : 'dunno';

        # Send action response
        print STDOUT "action=$action\n\n";
        %attr = ();
    }
}
